import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Routes from './route';

import {createBrowserHistory} from "history";
const history = createBrowserHistory();

ReactDOM.render(
  <Routes history={history} />,
  document.getElementById('root')
);

