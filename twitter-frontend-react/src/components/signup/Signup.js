import React from 'react';
import {withRouter} from 'react-router-dom';
import './Signup.css';
import TwitterLogin from "react-twitter-login";
import {saveUserDetail} from '../../services';

const CONSUMER_KEY = 'BHpJdDMYLgt4ZYTP585C0j9cY';
const CONSUMER_SECRET = '5L4Tiv7BYoUboKfAG3ySNtKlM6RlzIsMMDJ395uoZsYU4UglY3';
const CALLBACK_URL = 'http://localhost:3000';

class Signup extends React.Component {
constructor(props){
  super(props);
  this.state = {
    userData: [],
    isLoggedIn: false
  }
}
componentDidUpdate(prevProps, prevState) {
  debugger
  const { dispatch, redirectUrl } = this.props
  const isLoggingOut = prevState.isLoggedIn && !this.state.isLoggedIn
  const isLoggingIn = !prevState.isLoggedIn && this.state.isLoggedIn

  if (isLoggingIn) {
    this.props.history.push('/home');
  } else if (isLoggingOut) {
    // do any kind of cleanup or post-logout redirection here
    this.props.history.push('/');
  }
}
authHandler = async (err, data) => {
  // save user post call
  if(data){
    const response = await saveUserDetail(data);
    // response is true then success loggein 
    if(response){
      this.setState({userData: data, isLoggedIn: true});
      // sessionStorage.setItem(loggedIn, true);
      //this.props.history.push('/home');
    }
  }
};
render() {
  return (
    <div className="App">
    {!this.state.loggedIn ? 
    <div>
     <h1>Twitter Signup</h1>

     <TwitterLogin
      authCallback={this.authHandler}
      consumerKey={CONSUMER_KEY}
      consumerSecret={CONSUMER_SECRET}
      callbackUrl={CALLBACK_URL}
    /> 
    </div>
    : <div>
        <h1> Logged In </h1>
    </div>}
    </div>
  );
}  
}

export default withRouter(Signup);
