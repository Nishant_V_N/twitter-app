import React from 'react';
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom';  
import Signup from './components/signup/Signup';
import Home from './components/home/Home';

const Routes = () => (
<Router>
    <Switch>
        <Route path='/' exact component={Signup} />
        <Route path='/home' component={Home} />
    </Switch>
</Router>
)
export default Routes;