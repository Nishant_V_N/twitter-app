const express = require('express');
const http = require('http');
const PORT = process.env.PORT || 5000;
const morgan = require('morgan');
const cors = require('cors');

//create server
const app = express();
const server = http.createServer(app);

//routes
const router = require('./routes/user.js');

//middle wares
app.use(cors());
app.use(express.json())
app.use(morgan('combined'));
app.use(router);

//server port
server.listen(PORT, () => {
    console.log('server is running in port', PORT);
});