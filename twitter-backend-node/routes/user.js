const express = require('express');
const router = express.Router();
const mongooseClient = require('../config/mongo-db');

const {saveUser} = require('../helper-functions/index');

router.get('/', (req, res) => {
  res.send('server is running...');
});

router.post('/saveUserDetails', async (req, res) => {

  if(req.body && Object.keys(req.body).length){
    const saveStaus = await saveUser(req.body);

    console.log("saveStaus", saveStaus);
    if(saveStaus && saveStaus.error &&  saveStaus.status === 'error'){
      res.json({
        staus: 400,
        error: true,
        success: false,
        message: 'failed to save user details.'
      });
    }
    res.json({
      staus: 200,
      error: null,
      success: true,
      message: 'user details saved successfully'
    });

  } else {
    res.json({
      staus: 200,
      error: true,
      success: false,
      message: 'user details required'
    });
  }
});

module.exports = router;