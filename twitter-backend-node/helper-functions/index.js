const Users = require('../models/user-model');

const saveUser = userData => {
const  {oauth_token, oauth_token_secret, screen_name,
user_id} = userData;
const users = new Users({
    oauth_token,
    oauth_token_secret,
    screen_name,
    user_id,
    create_date: new Date(),
   });

   Users.countDocuments({user_id: user_id}, function (err, count){ 
    if(count>0){
        //document exists });
        return {
            data: user_id,
            error: null,
            status: 'success',
            message: 'exixting user'
        }
    }
    
    users.save()
       .then(item => { 
        return {
            data: item,
            error: null,
            status: 'success',
            message: 'saved to db'
        }
       })
       .catch(err => {
           console.log("Error", err);
        return {
            data: err,
            error: null,
            status: 'error',
            message: 'error while saving to db'
        }
       });
}); 

};

module.exports = {saveUser};