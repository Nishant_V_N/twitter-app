const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    oauth_token: String,
    oauth_token_secret: String,
    screen_name: String,
    user_id: String,
    create_date: Date
});

const Users = mongoose.model('users', userSchema);

module.exports = Users;